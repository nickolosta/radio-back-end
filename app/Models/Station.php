<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
  protected $table = 'stations';
  protected $fillable = [
    'id',
    'name',
    'description',
    // 'genre_id',
    'logoFileInfo',
    'urlSound',
    'countView',
    'URL_logo',
    'place',
    'language_id',
    'contacts',
    'site',
    'email'
  ];

  public function genres()
  {
    return $this->belongsToMany('App\Models\Genre', 'stations_genres')
      ->withTimestamps();
  }

  public function language()
  {
    return $this->belongsTo('App\Models\Language');
  }
  public function favorite()
  {
    return $this->belongsTo('App\Models\Favorite', 'station_id', 'id');
  }
}
