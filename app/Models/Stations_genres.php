<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stations_genres extends Model
{
    protected $table = 'stations_genres';
    protected $fillable = [
        'genre_id',
        'station_id',
    ];
    public function stations()
    {
        return $this->hasMany('App\Models\Station');
    }
    public function genres()
    {
        return $this->hasMany('App\Models\Genre');
    }
}
