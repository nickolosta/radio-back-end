<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use Validator;

class Confirm extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'confirm_registration';
    protected $fillable = ['name','last_name','email','hash','password'];
    protected $guarded = ['id'];

    // оставим только дату создания в модели
    public $timestamps = false;
    public static function boot()
    {
        static::creating(function ($model) {
            $model->created_at = $model->freshTimestamp();
        });
    }
}
