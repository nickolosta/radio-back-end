<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $table = 'favorites';
    protected $fillable = [
        'station_id',
        'user_id'
    ];
  public function station()
  {
    return $this->hasOne('App\Models\Station', 'id', 'station_id');
  }
}
