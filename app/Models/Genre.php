<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $table = 'genres';
    protected $fillable = [
        'name',
        'parentId'
    ];
    public function stations()
    {
        return $this->belongsToMany('App\Models\Station','stations_genres')
            ->withTimestamps();
    }
}
