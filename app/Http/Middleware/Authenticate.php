<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Http\API\Data\Token as model_token;
class Authenticate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        // проверим пришли ли заголовки токена
        $token = $request->header('login-token');
        $token_user_id = $request->header('login-token-user');
        if($token!=null && $token_user_id!=null){
            $token_model = model_token::find(intval($token_user_id),$token);
            // если токен найден пробуем авторизоваться буз куки и сессий
            if($token_model){
                return $next($request);
            }
        }

        if (Auth::guard($guard)->guest()) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            } else {
                return redirect()->guest('login');
            }
        }
        return $next($request);
    }
}
