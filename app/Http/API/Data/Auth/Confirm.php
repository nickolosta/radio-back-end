<?php

namespace App\Http\API\Data\Auth;

use App\Models\Auth\Confirm as model_confirm;
use Validator;
use Mail;
use App\Log;

class Confirm {
    /*
    |--------------------------------------------------------------------------
    |  подтверждение регистрации
    |--------------------------------------------------------------------------
    | В этом классе содержиться бизнес логика этой сущности
    */

    // валидация данных с правилами
    /**
     * Возвращает валидацию с полями
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected static function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:30',
             'last_name' => 'required|max:30',
            'email' => 'required|email|max:30|unique:users',
            'password' => 'required|min:6',
        ]);
    }


    /**
     * Создает новую запись в таболице confirm_registration,
     * возвращает модель с данными
     *
     * @param  array $raw_data
     * @param  boolean $validate
     * @return Model Confirm
     *
     * @throws Exception
     */
    // создание новой записи в таблице подтверждение регистрации
    public static function insert(array $raw_data,$validate = true){
        // валидация данных если нужно
        if($validate){
            $validator = Confirm::validator($raw_data);
            if($validator->fails()){
                throw new Exception('Данные не прошли валидацию');
                return ;
            }
        }
        $data = [
            'name' => $raw_data['name'],
            'email' => $raw_data['email'],
            'password' => bcrypt($raw_data['password']),
            'last_name'=>$raw_data['last_name'],
            'hash'=>str_random(20)
        ];

        $model = new model_confirm($data);
        $model->save();
        return $model;
    }

    // отправка сообщения о подтверждении регистрации
    /**
     * Отправляет сообщение с подтверждением регистрации на почту пользоватяля
     * Возвращает истину при успешной отправке сообщения
     * @param  string $from
     * @param  string $email
     * @param  string $id
     * @param  string $hash
     * @return boolean
     */
    public  static function sendConfirmToEmail($from,$email,$id,$hash){

        // данные для уникального кеша
        $data = ['key'=>$hash,'id'=>$id];
        // отправка почтового уведомления
        $sent = Mail::send('emails.confirm_registration', $data, function($message) use ($email,$from)
        {
            $message->from($from);
            $message->subject('Сервис Договорились - подтверждение вашей регистрации');
            $message->to($email);
        });
        if(!$sent){
            \Log::error('Письмо '.$email.' не отправлено');
            return false;
        }else{
            return true;
        }
    }


    // подтверждение регистрации и создание нового пользователя
    /**
     * Создает нового пользователя если он не существует
     * и есть запись в таблице confirm_registration
     * возвращает модель User с данными или null
     * @param  string $id_confirm
     * @param  string $hash
     * @return Model User
     */
    public static function confirm($id_confirm,$hash){

        // ищем пользователя с id записи и хешем
        $user_confirm = model_confirm::where('id',$id_confirm)->where('hash',$hash)->first();
        // если нет - то возвращаем пустое значение
        if($user_confirm==null){
            return null;
        }
        // добавляем пользователя
        $data = [
            'name' => $user_confirm->name,
            'email' =>$user_confirm->email,
            'password' =>$user_confirm->password,
            'last_name'=>$user_confirm->last_name
        ];
        //dd($data);
        $user_model = User::create($data);
        // если пользователь не создан вернем null
        if(!$user_model){
            return null;
        }
        $user_confirm->delete();
        return $user_model;
    }

}