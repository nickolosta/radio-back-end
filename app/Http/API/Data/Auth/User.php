<?php

namespace App\Http\API\Data\Auth;

use Validator;
use Mail;
use App\User as Model_user;
use Illuminate\Support\Facades\Hash;

class User {
    /*
    |--------------------------------------------------------------------------
    | пользователь
    |--------------------------------------------------------------------------
    | В этом классе содержиться бизнес логика этой сущности
    */

    // создание нового пользователя
    /**
     * Создает нового пользователя<br>
     * возвращает модель User с данными<br>
     * @param  array $data
     * @return User
     */
    public  static  function create(array $data){
        $user= Model_user::create($data);
        return $user;
    }

    // находит или создает нового пользователя из socialite
    /**
     * находит или создает нового пользователя из socialite
     * возвращает модель User с данными
     * @param  socialite_User $oauthData
     * @return User
     */
    public static function findByEmailOrCreateFromOAuthData($oauthData,$from_ajax=false){
        $user = Model_user::where('email',$oauthData->email)->first();
        if ($user!=null) {
            return $user;
        }
        // сгенерируем случайный пароль
        $random_password = str_random(8);
        $password_hash = User::getPasswordHash($random_password);
        //отправим пользователю уведомление о пароле на почту
        $sent = User::sendEmailWithRandomPassword($random_password,$oauthData->email);
        // если письмо отправлено, то создадим запись
        if($sent){
            if(isset($oauthData->user['first_name'])){
                $name = $oauthData->user['first_name'];
            }else{
                $name = explode(' ',$oauthData->name)[0];
            }
            if(isset($oauthData->user['last_name'])){
                $last_name = $oauthData->user['last_name'];
            }else{
                $last_name ='';
            }
            $data = [
                'name'=>$name,
                'last_name'=>$last_name,
                'email'=>$oauthData->email,
                'password'=>$password_hash
            ];
            $user =  User::create($data);
            return $user;
        }else{
            if($from_ajax){
                return false;
            }else{
                return redirect('/505');
            }
        }
    }

    /**
     * получение hash пароля
     * используется для oauth
     * @param  $string User model $oauthData
     * @return string password
     */
    public static function getPasswordHash($string){
        return bcrypt($string);
    }

    /**
     * получаем from из настроек приложения
     * @return string from_email
     */
    public static function getFromEmail(){
        $from = config("mail.from.address");
        return $from;
    }

    // отправка сообщения c random password
    // используется для OAuth;
    /**
     * отправка сообщения c random password
     * в случаи упспеха возвращает истину
     * @return boolean
     */
    public static function sendEmailWithRandomPassword($random_password,$email){
        $from = User::getFromEmail();
        $data = ['random_password'=>$random_password,'email'=>$email];
        $sent = Mail::send('auth.emails.oauth_random_password', $data, function($message) use ($email,$from)
        {
            $message->from($from);
            $message->subject('Сервис Договорились - данные для авторизации');
            $message->to($email);
        });
        if( ! $sent){
            Log::error('Письмо '.$email.' не отправлено');
        }else{
            return true;
        }
    }

    /**
     * получаем данные пользователя по id
     * возвращает модель с данными или false
     * @return \app\User|false
     */
    public static function getUserById($id){
        $id_int = intval($id);
        $model = Model_user::where(['id'=>$id_int])->select('id','name','last_name','email')->first();
        if($model==null){
            return false;
        }
        return $model;
    }
    /**
     * получаем всех пользователей
     * @return array
     */
    public  static  function getAllUsers(){
        $result = Model_user::get();
        if($result!=null){
            return $result->toArray();
        }else{
            return [];
        }
    }
    /** получить пользователя по email и хешу пароля
     * @param string $email
     * @param string $password
     * @return boolean | \App\User
     */
    public  static  function getUserByEmailAndPassword($email,$password){
        $user = Model_user::where(['email'=>$email])->first();
        if($user!=null){
            $validCredentials = Hash::check($password, $user->password);
            if ($validCredentials) {
                return $user;
            }
        }
        return false;
    }
}