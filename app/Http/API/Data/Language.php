<?php

namespace App\Http\API\Data;

use App\Models\Language as modelLanguage;
use Illuminate\Support\Facades\Validator;

class Language
{
    /**
     * <h4>Validation</h4>
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected static function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:30'
        ]);
    }

    /** <h4>Make new language </h4>
     * @param  array  $data
     * @return \App\Models\Genre | boolean
     */
    public  static function create($data){
        $validator = Language::validator($data);
        if(!$validator->fails()) {
          $countGenres = modelLanguage::where(['name' => $data['name']])->count();
          
          if($countGenres == 0) {
              $newGenre = modelLanguage::create($data);
              return $newGenre;
          }else {
            return ["error" => "dublicate_genre"];
          }
        }
        return false;
    }
}