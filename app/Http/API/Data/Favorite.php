<?php

namespace App\Http\API\Data;

use App\Models\Favorite as modelFavorite;
use Illuminate\Support\Facades\Validator;

class Favorite
{
    /**
     * <h4>Validation</h4>
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected static function validator(array $data)
    {
        return Validator::make($data, [
            'station_id' => 'required',
            'user_id' => 'required'
        ]);
    }

    /** <h4>Make new favorite </h4>
     * @param  array  $data
     * @return \App\Models\Favorite | boolean
     */
    public  static function create($data){
        $validator = Favorite::validator($data);
        if(!$validator->fails()) {
          $countFavorite = modelFavorite::where(['station_id' => $data['station_id'],'user_id' => $data['user_id']])->count();
          
          if($countFavorite == 0) {
              if((int) $data['station_id'] > 0 && (int) $data['user_id'] > 0){
                $newFavorite = modelFavorite::create($data);
                return $newFavorite;
              }
          } else {
            return ["error" => "dublicate_favorite"];
          }
        }
        return false;
    }
  /** <h4>Get list of favorites </h4>
   * @param array $data
   * @return array
   */
  public  static function getList($data, $offset, $limit){
    $favorites = modelFavorite::where(['user_id' => $data['user_id']])->with('station')->offset($offset*$limit)->limit($limit)->get()->map(function ($model, $key) {
      return $model->station;
    })->toArray();
    return $favorites;
  }
  /** <h4>Remove favorite station </h4>
   * @param array $data
   * @return boolean
   */
  public  static function remove($data){
    $favorite = modelFavorite::where(['user_id' => $data['user_id'], 'station_id' => $data['station_id']])->first();
    return $favorite->delete();
  }
}