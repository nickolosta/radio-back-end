<?php

namespace App\Http\API\Data;

use App\Models\Station as modelStation;
use App\Models\Genre;
use App\Models\Stations_genres;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Storage;
use Carbon\Carbon;

class Station {

  /**
   * <h4>Validation</h4>
   *
   * @param  array  $data
   * @return \Illuminate\Contracts\Validation\Validator
   */
  protected static function validator(array $data) {
    return Validator::make($data, [
                'name' => 'required|max:100',
                'description' => 'max:1000',
                'genres' => 'required',
                'logo' => 'nullable|mimes:jpeg,bmp,png',
                //'logoJSON' => 'required',
                //'urlSound' => 'required'
    ]);
  }

  public static function createFile($file){;
    $now = Carbon::now();
    $path = $now->year.'_'.ceil($now->month/3).'q';
    $storageResult = Storage::put($path, $file);
    $mime = Storage::mimeType($storageResult);
    $path_arr = explode('/', $path);
    $fileInfo = [
      'storageFolder' => $path,
      'name' => end($path_arr),
      'path' => $storageResult,
      'file_size' => Storage::size($storageResult),
      'realFileName' => 'logoStation.'.explode('/', $mime)[1],
      'file_mime' => $mime
    ];
    return $fileInfo;
  }
  public static function getLogoStation($stationId){
    $station = modelStation::where(['id'=>$stationId])->first();
    $dataFile = json_decode($station->logoFileInfo);
    $fileURL = Storage::get($dataFile->path);
    return $fileURL;
  }
  /** <h4>Make new station </h4>
   * @param  array  $data
   * @return \App\Models\Station | boolean
   */
  public static function create($data) {
    $validator = Station::validator($data);
    if (!$validator->fails()) {

      // logo file
      $logoFile = Station::createFile($data['logo']);
      $data['logoFileInfo'] = json_encode($logoFile);

      //$data['logoFileInfo'] = '';
      $data['countView'] = (int) $data['countView'];
      $countStations = modelStation::where(['name' => $data['name']])->count();
      if ($countStations == 0) {
        $genres = trim($data['genres']);
        $data = array_filter($data,function($genre) {return $genre != 'genres';});
        $newStation = modelStation::create($data);
        $newStation->genres()->attach(explode(",", $genres));
        return $newStation;
      } else {
        return ["error" => "dublicate_station"];
      }
    } else {
      return ["error" => ["code" => "validation_data_error", 'errors' => $validator->getMessageBag()]];
    }
  }
    /** <h4>Make new station </h4>
     * @param  array  $data
     * @return \App\Models\Station | boolean
     */
    public static function edit($data) {
        $validator = Station::validator($data);
        if(!isset($data['id'])){
            return ["error" => "data not have id field"];
        }
        $station = modelStation::where(['id'=>intval($data['id'])])->first();
        if (!$validator->fails()) {
            // logo file
            $logoFile = Station::createFile($data['logo']);
            $data['logoFileInfo'] = json_encode($logoFile);
            //$data['logoFileInfo'] = '';
            $data['countView'] = (int) $data['countView'];
            if ($station) {
                $station->fill($data);
                $genres = trim($data['genres']);
                $station->genres()->sync(explode(",", $genres));
                $station->save();
                return $station;
            } else {
                return ["error" => "not found station"];
            }
        } else {
            return ["error" => ["code" => "validation_data_error", 'errors' => $validator->getMessageBag()]];
        }
    }
  /** <h4>List stations </h4>
   * @param  int|string $genre_id
   * @return array
  */
  public static function getListStations($genre_id, $offset, $limit){
      $genres_tree = DB::table('genres AS t1')
          ->select('t1.id AS lev1', 't2.id as lev2','t3.id as lev3', 't4.id as lev4')
          ->leftJoin('genres AS t2', 't2.parentId', '=', 't1.id')
          ->leftJoin('genres AS t3', 't3.parentId', '=', 't2.id')
          ->leftJoin('genres AS t4', 't4.parentId', '=', 't3.id');
      if(intval($genre_id) != 0){
          $genres_tree = $genres_tree->where('t1.id','=', $genre_id);
      }
      $genres_tree = $genres_tree->get()->toArray();
      $arrIdGroups = [];
      foreach ($genres_tree as $group_tree) {
          foreach ($group_tree as $id) {
              array_push($arrIdGroups,$id);
          }
      }
      $arrIdGroups = array_unique($arrIdGroups);
      $stations = modelStation::whereHas('genres', function($q) use($arrIdGroups) {
              $q->whereIn('id', $arrIdGroups);
          })->offset($offset*$limit)->limit($limit)->get()->toArray();
      //$stations = modelStation::whereIn('genre_id', array_unique($arrIdGroups))->offset($offset*$limit)->limit($limit)->get()->toArray();
      return $stations;
  }
    /** <h4>List stations </h4>
     * @return array
     */
    public static function getlistLastAddedStations(){
        $stations = modelStation::orderBy('created_at','desc')->limit(16)->get()->toArray();
        return $stations;
    }

    public static function getDetail($stationId){
        $station = modelStation::where(['id'=>$stationId])->with('language')->with('genres')->first();
        // places
        if($station->place) {
            if(strtolower($station->place) == 'usa'){

            }
        }
        return $station;
    }

}
