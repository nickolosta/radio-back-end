<?php

namespace App\Http\API\Data;

use App\Models\Genre as modelGenre;
use Illuminate\Support\Facades\Validator;

class Genre
{
    /**
     * <h4>Validation</h4>
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected static function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:30',
            'parentId' => 'required'
        ]);
    }

    /** <h4>Make new genre </h4>
     * @param  array  $data
     * @return \App\Models\Genre | boolean
     */
    public  static function create($data){
        $validator = Genre::validator($data);
        if(!$validator->fails()) {
          $countGenres = modelGenre::where(['name' => $data['name']])->count();
          
          if($countGenres == 0) {
              if((int) $data['parentId'] > 0){
                $parentGenre = modelGenre::where(['id' => $data['parentId']])->first();
                if(!$parentGenre){
                  return ["error" => "parent_genre_not_found"];
                }
              }
              $newGenre = modelGenre::create($data);
              return $newGenre;
          }else {
            return ["error" => "dublicate_genre"];
          }
        }
        return false;
    }
    /** <h4>List genres </h4>
     * @param  int|string $parent_id
     * @return array
     */
    public static function getListGenres($parent_id){
        $parent_id_int = intval($parent_id);
        $genres = modelGenre::where(['parentId' => $parent_id_int])->get()->toArray();
        return $genres;
    }
    /** <h4>get genre by id</h4>
     * @param  int|string $genre_id
     * @return \App\Models\Genre
     */
    public static function getGenre($genre_id){
        $genre_id_int = intval($genre_id);
        $genre = modelGenre::where(['id' => $genre_id_int])->first();
        return $genre;
    }
}