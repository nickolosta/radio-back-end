<?php
/**
 * Created by PhpStorm.
 * User: nick
 * Date: 14.06.16
 * Time: 16:30
 */

namespace App\Http\API\Data;

use App\Models\Token as modelToken;


class Token
{
  /** <h4>Создает экземпляр токена </h4>
   *  для пользователя.<br> Возвращает массив с токеном и id пользователя
   * @param int $id_user
   * @return array
   */
  public static function create($id_user)
  {
    $old_token = modelToken::where(['user_id' => $id_user])->first();
    // если существует старый токен, то проверим время жизни,
    // если оно прошло удалим токен и создадим новый
    if ($old_token != null) {
      $old_token_time = strtotime($old_token->created_at);
      $current_time = time();
      $differenceInSeconds = $current_time - $old_token_time;
      $time_out_seconds = intval(config("app.timeout_token_login"));
      if ($differenceInSeconds > $time_out_seconds) {
        $old_token->delete();
      } else {
        // возвращаем старый токен
        return ['token' => $old_token->api_token, 'id_user' => $id_user];
      }
    }
    $model = new modelToken();
    $model->user_id = $id_user;
    $token = str_random(60);
    $model->api_token = $token;
    $model->save();
    return ['token' => $token, 'id_user' => $id_user];
  }

  /** поиск токена
   * возвращает модель токена или false
   * @param string $token
   * @return  boolean|model
   */
  public static function find($token)
  {
    $token = modelToken::where(['api_token' => $token])->first();
    if ($token != null) {
      // если есть токен, то проверим устаревший ли он
      $token_time = strtotime($token->created_at);
      $current_time = time();
      $differenceInSeconds = $current_time - $token_time;
      $time_out_seconds = intval(config("auth.timeout_token_login"));
      if ($differenceInSeconds < $time_out_seconds) {
        return $token;
      }
    }
    return false;
  }
}