<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\Http\API\Data\Auth\User;
use App\Http\API\Data\Token;


class TokenController extends Controller
{
    public function __construct()
    {

    }
    public function getUserData(Request $request) {
        $token = $request->header('t');
        //dd($token);
        $tokenModel = Token::find($token);
        if($tokenModel){
            $user = User::getUserById($tokenModel->user_id);
            if($user) {
                return response()->json(['success' => true,'data'=>['user'=>$user] ]);
            }
        }
        return response()->json(['success' => false, 'data' => null ]);
    }
    public  function getToken(Request $request) {
        $data = $request->all();
        if(isset($data['email']) && isset($data['password'])) {
            $user = User::getUserByEmailAndPassword($data['email'],$data['password']);
            if ($user)
            {
                $result = Token::create($user->id);
                $result['user'] = $user->toArray();
                return response()->json(['success' => true,'data'=> $result]);
            }
        }
        return response()->json(['success' => false]);
    }

    public  function  checkToken(Request $request) {
        $token = $request->header('login-token');
        $tokenModel = Token::find($token);
        if($tokenModel){
            $user = User::getUserById($tokenModel->user_id);
            if($user) {
                return response()->json(['success' => true,'user'=>$user]);
            }
        }
        return response()->json(['success' => false]);
    }

}
