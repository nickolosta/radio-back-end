<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\API\Data\Station;

class StationController extends Controller
{
  public function Create(Request $request)
  {
    $data = $request->all();
    $station = Station::create($data);
    if ($station) {
      if (isset($station['error'])) {
        return response()->json(['success' => false, 'error' => $station['error']]);
      }
      return response()->json(['success' => true, 'station' => $station]);
    }
    return response()->json(['success' => false]);
  }

  public function Edit(Request $request)
  {
    $data = $request->all();
    $station = Station::edit($data);
    if ($station) {
      if (isset($station['error'])) {
        return response()->json(['success' => false, 'error' => $station['error']]);
      }
      return response()->json(['success' => true, 'station' => $station]);
    }
    return response()->json(['success' => false]);
  }

  public function getlistStations(Request $request)
  {
    $data = $request->all();
    if (isset($data['genre_id'])) {
      $stations = Station::getListStations($data['genre_id'], $data['offset'], $data['limit']);
      return response()->json(['success' => true, 'data' => ['stations' => $stations]]);
    }
    return response()->json(['success' => false]);
  }

  public function getlistLastAddedStations(Request $request)
  {
    $stations = Station::getlistLastAddedStations();
    return response()->json(['success' => true, 'data' => ['stations' => $stations]]);
  }

  public function getDetail(Request $request)
  {
    $data = $request->all();
    if (isset($data['station-id'])) {
      $station = Station::getDetail($data['station-id']);
      if ($station) {
        return response()->json(['success' => true, 'data' => ['station' => $station]]);
      }
    }
    return response()->json(['success' => false]);
  }

  public function getLogoStation(Request $request)
  {
    $data = $request->all();
    if (isset($data['station-id'])) {
      $file = Station::getLogoStation($data['station-id']);
      if ($file) {
        return response($file, 200)->header('Content-Type', 'image/png');
      }
    }
    return response()->json(['success' => false]);
  }
  //2018_2q/GnJj8O5uq481ZMPS1ADRSCqmDb28xNz4yciPWa8G.png
}
