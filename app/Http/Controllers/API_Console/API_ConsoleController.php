<?php

namespace App\Http\Controllers\API_Console;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use Socialite;
use Illuminate\Support\Facades\Auth;
use App\Http\API\Data\Auth\UserOAuth;

class API_ConsoleController extends Controller
{
   public  function  index(){
       return view('api_console');
   }
}
