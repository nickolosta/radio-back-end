<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\API\Data\Favorite;
use App\Http\API\Data\Token;
use Cookie;

class FavoritesController extends Controller
{
  public function create(Request $request)
  {
    $data = $request->all();
    $tokenData = Token::find($request->header('t'));
    if ($tokenData) {
      $data['user_id'] = $tokenData->user_id;
      $favorite = Favorite::create($data);
      if ($favorite) {
        if (isset($favorite['error'])) {
          return response()->json(['success' => false, 'error' => $favorite['error']]);
        }
        return response()->json(['success' => true, 'favorite' => $favorite]);
      }
    }
    return response()->json(['success' => false]);
  }
  public function getListFavorites(Request $request)
  {
    $data = $request->all();
    $tokenData = Token::find($request->header('t'));
    if ($tokenData) {
      $data['user_id'] = $tokenData->user_id;
      $favorites = Favorite::getList($data, 0, 20 );
      return response()->json(['success' => true, 'data'=>['favorites' => $favorites]]);
    }
    return response()->json(['success' => false]);
  }
  public function remove(Request $request)
  {
    $data = $request->all();
    $tokenData = Token::find($request->header('t'));
    if ($tokenData) {
      $data['user_id'] = $tokenData->user_id;
      $isRemoved = Favorite::remove($data);
      if($isRemoved){
        return response()->json(['success' => true]);
      }
    }
    return response()->json(['success' => false]);
  }
}
