<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\API\Data\Auth\Confirm;
use Auth;
use Illuminate\Auth\UserInterface;
use App\User as model_user;
class ConfirmController extends Controller
{

    protected $redirectTo = '/home';

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function confirm($id_confirm,$hash){
        $model_user = Confirm::confirm($id_confirm,$hash);
        // если пользователь не найден вернем 404 страницу
        if($model_user==null){
            return redirect('404');
        }
        // авторизуемся и перенаправляем на страницу
        if (Auth::loginUsingId($model_user->id)) {
            return response()->json(['success' => true]);
        }
    }

}
