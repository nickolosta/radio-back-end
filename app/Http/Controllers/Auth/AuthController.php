<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;

use App\Http\API\Data\Auth\Confirm;
use Socialite;
use Illuminate\Support\Facades\Auth;
use App\Http\API\Data\Auth\UserOAuth;

class AuthController extends Controller
{

    protected $redirectTo = '';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
        if (\Session::get('url.intended')!=null)
        {
            $this->redirectAfterLogout = \Session::get('url.intended'); // Send back to previous url if possible
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'last_name' => 'required|max:30',
            'name' => 'required|max:30',
            'email' => 'required|email|max:30|unique:users',
            'password' => 'required|confirmed|min:6|regex:/^(?=.*\d)(?=.*[a-z])(?=(?:.*[\da-zA-Z]){3})/',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        // не используем этот метод
        dd();
    }

    /**
     * получаем from из настроек приложения
     */
    public  function getFromEmail(){
        $from = config("mail.from.address");
        return $from;
    }

    /**
     * Подтверждение регистрации
     * $sendmail_message = перевод на страницу html с выводом сообщения
     */
    public function register(Request $request,$from_ajax=false)
    {
        // валидация данных запроса
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return response()->json(['success' => false,'errors'=>$validator->errors()]);
        }
        // через API создадим запись в БД
        $model = Confirm::insert($request->all(),false);
        // отправим уведомление по почте
        Confirm::sendConfirmToEmail($this->getFromEmail(),$request->all()['email'],$model->id,$model->hash);
       // if(!$from_ajax){
            // перенавправим на страницу сообщения
      //      return redirect('/message/email/'.$request->all()['email']);
       // }else{
            return response()->json(['success' => true,'data'=>['email'=>$request->all()['email']] ]);
      //  }
    }

    /** регистрация через ajax */
    public function registerAjax(Request $request){
        return $this->register($request,true);
    }

}
