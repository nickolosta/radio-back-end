<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Socialite;
use App\Http\API\Data\Auth\User;
use Illuminate\Support\Facades\Auth;
use App\Http\API\Data\Token;
class OAuthController extends Controller
{
    public function __construct()
    {
       $this->middleware('guest');
    }

    public function OAuthByToken(Request $request){
        $data = $request->all();
        try {
            $data = Socialite::driver($data['driver'])->user();
            $user = $this->FindOrCreateUser($data);
            if($user){
                $token = Token::create($user->id);
                return response()->json(['success' => true,'data'=>$token]);
            }
        } catch (Exception $e) {
        }
        return response()->json(['success' => false]);
    }

    public function OAuthRedirect($provider)
    {
        //Проверка socialite
        return Socialite::driver($provider)->redirect();
    }

    public function OAuthHandle($provider,Request $request,$data = null)
    {
        // проверям на различные ошибки
        $data = $request->all();
       // dd($data);
        $errors =isset($data['error']);
        if($errors){
            return redirect('/login');
        }
        $data = Socialite::driver($provider)->user();
        $user = $this->FindOrCreateUser($data);
        if($user){
            $prevPage = \Session::get('url.intended');
            // авторизуемся и перенаправляем на страницу
            if (Auth::loginUsingId($user->id)) {
                if(is_null($prevPage)){
                    return redirect('/settings');
                }else{
                    return redirect($prevPage);
                }
            }
        }else{
            return redirect('/login');
        }
    }
    /** авторизируемся или регистрируем пользователя*/
    protected function FindOrCreateUser ($data,$from_ajax=false){
        if($from_ajax){
            return false;
        }else{
            if($data['email']==null){
                return redirect('/login');
            }
        }
        return User::findByEmailOrCreateFromOAuthData($data,$from_ajax);
    }
}
