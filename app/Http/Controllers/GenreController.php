<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\API\Data\Genre;

class GenreController extends Controller
{
  public function Create(Request $request)
  {
    $data = $request->all();
    $genre = Genre::create($data);
    if ($genre) {
      if (isset($genre['error'])) {
        return response()->json(['success' => false, 'error' => $genre['error']]);
      }
      return response()->json(['success' => true, 'genre' => $genre]);
    }
    return response()->json(['success' => false]);
  }

  // get list genres
  public function getListGenres(Request $request)
  {
    $data = $request->all();
    $parentRoot = false;
    if (isset($data['genre_id'])) {
      $genres = Genre::getListGenres($data['genre_id']);
      if (intval($data['genre_id']) == 0) {
        $parentRoot = true;
      }
      if (count($genres) == 0) {
        $parent = Genre::getGenre($data['genre_id']);
        if ($parent) {
          if($parent['parentId'] == 0){
            $parentRoot = true;
          }
          $genres = Genre::getListGenres($parent['parentId']);
        } else {
          $genres = Genre::getListGenres(0);
          $parentRoot = true;
        }
      }
      if(!$parentRoot){
        array_unshift($genres, ['name' => 'All_genres', 'id' => 0]);
      }
      return response()->json(['success' => true, 'data' => ['genres' => $genres, 'parentRoot' => $parentRoot]]);
    }
    return response()->json(['success' => false]);
  }

// get genre
  public function getGenre(Request $request)
  {
    $data = $request->all();
    if (isset($data['genre_id'])) {
      $genre = Genre::getGenre($data['genre_id']);
      return response()->json(['success' => true, 'data' => ['genre' => $genre]]);
    }
    return response()->json(['success' => false]);
  }
}
