<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\API\Data\Language;

class LanguageController extends Controller
{
     public  function  Create(Request $request) {
        $data = $request->all();
        $language = Language::create($data);
        if($language){
          if(isset($language['error'])){
             return response()->json(['success' => false, 'error'=> $language['error']]);
          }
          return response()->json(['success' => true, 'genre' => $language]);
        }
        return response()->json(['success' => false]);
    }
}
