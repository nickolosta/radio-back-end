<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <title>Консоль</title>
    <meta name="_token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{ URL::asset('css/jquery.json-viewer.css') }}" />
</head>
<body>
<style>
    body,html
    {
        width: 100%;
        height: 100%;
        text-align: center;
        box-sizing: border-box;
        background: #333;
        font-family: Arial;
        margin: 0;
    }
    #container{
        width: 90%;
        min-width: 1150px;
        height: 90%;
        border: 1px solid #e7e7e7;
        display: inline-block;
        box-shadow: 0 5px 15px rgba(0,0,0,0.5);
        background: #f9f9f9;
        position: relative;
        overflow-y: auto;
    }
    #logo
    {
        position: absolute;
        right: 10px;
        top: 10px;
        color: #5bbc2e;
        font-size: 18px;
    }
    #tabs
    {
        display: block;
        width: 100%;
        margin-top: 50px;
        text-align: left;
        padding: 15px 10px;
        box-sizing: border-box;
        border-bottom: 1px solid #e7e7e7;
        height: 40px;
        vertical-align: bottom;
    }
    .tab_bt
    {
        padding: 5px 10px;
        border-left: 1px solid #e7e7e7;
        border-right: 1px solid #e7e7e7;
        border-top: 1px solid #e7e7e7;
        cursor: pointer;
    }
    .tab_bt.active
    {
        background: #5bbc2e;
    }
    .tab
    {
        display: none;
        padding: 30px;
        box-sizing: border-box;
        text-align: left;
    }
    .tab.active
    {
        display: block;
    }
    input,select
    {
        width: 300px;
        min-height: 30px;
    }
    #URL_res_container
    {
        position: absolute;
        left: 10px;
        top:20px;
    }
    input#URL_res
    {
        min-height: 20px !important;
        line-height: 20px;
    }
    #data_table
    {
        margin-top: 5px;
        width: 100%;

    }
    #data_table thead
    {
        background: #e7e7e7;
        border: 1px solid #333;
        text-align: center;
    }
    #data_table td{

    }
    #post_result_column,
    #get_result_column
    {
        width: 60%;
    }
    .post_fields_cont,
    .get_fields_cont
    {
        width: 100%;
        overflow-y: auto;
        padding: 10px;
        box-sizing: border-box;
    }
    .post_fields_cont:nth-of-type(2),
    .get_fields_cont:nth-of-type(2)
    {
        border-left:  1px solid #e7e7e7;
    }
    #socket_send_data,
    #post_send_data,
    #get_send_data
    {
        padding: 10px;
        background: #5bbc2e;
        color: #333;
        cursor: pointer;
    }
    #post_result,
    #get_result
    {
        padding-left: 20px;
        box-sizing: border-box;
    }
    #token_container
    {
        position: absolute;
        right: 364px;
        top:10px;
        font-size: 12px;
    }
    #token_container input
    {
        min-height: 20px;
        line-height: 20px;
        font-size: 13px;
    }
    #login-token
    {
        margin-left: 29px;
        margin-bottom: 3px;
    }
    #reset_token
    {
        position: absolute;
        right: -90px;
        bottom: 0;
    }
</style>

<div id="container">
    <div id="token_container">
        Request headers:<br>
        <b>login-token</b><input id="login-token" type="text"><br>
        <b>login-token-user</b><input id="login-token-user"  type="text">
        <Button id="reset_token">Сбросить</Button>
    </div>
    <span id="logo">Консоль API</span>
    <div id="URL_res_container">URL: <input id="URL_res"></div>
    <div id="tabs">
        <a tab="get" class="active tab_bt">GET</a>
        <a tab="post" class="tab_bt">POST</a>
        <a tab="socket" class="tab_bt">Socket</a>
    </div>
    <div id="tab_get" class="active tab">
        Запрос:
        <select id="get_select_route">
            <!-- data -->
        </select><br>
        <table id="data_table">
            <thead>
            <th>Поля</th>
            <th>Ответ</th>
            </thead>
            <tbody>
            <tr>
                <td id="get_fields_column" valign="top">
                    <div class="get_fields_cont" id="get_fields"></div>
                </td>
                <td id="get_result_column" valign="top">
                    <div id="get_result" class="get_fields_cont">

                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div id="tab_post" class="tab">
        Запрос:
        <select id="post_select_route">
            <!-- data -->
        </select><br>
        <table id="data_table">
            <thead>
            <th>Поля</th>
            <th>Ответ</th>
            </thead>
            <tbody>
            <tr>
                <td id="get_fields_column" valign="top">
                    <div class="post_fields_cont" id="post_fields"></div>
                </td>
                <td id="get_result_column" valign="top">
                    <div id="post_result" class="post_fields_cont">

                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div id="tab_socket" class="tab">
        Emit:<br>
        <select id="socket_select_route">
            <!-- data -->
        </select><br><br>
        emit_name:<b id="emit_name"></b><br><br>
        <table id="data_table">
            <thead>
            <th>Emit</th>
            <th>Event</th>
            </thead>
            <tbody>
            <tr>
                <td id="socket_fields_column" valign="top">
                    <div class="socket_fields_cont" id="socket_fields"></div>
                </td>
                <td id="socket_result_column" valign="top">
                    <div id="socket_result_info"></div>
                    <div id="socket_result" class="socket_fields_cont">

                    </div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

</div>
<script type="text/javascript" src="{{ URL::asset('js/lib/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/lib/socket.io-1.4.5.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/lib/jquery.json-viewer.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/api/console/main.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/api/console/tabs.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/api/console/data.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/api/console/get.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/api/console/post.js') }}"></script>
<script type="text/javascript" src="{{ URL::asset('js/api/console/socket.js') }}"></script>
</body>
</html>