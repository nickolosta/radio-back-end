<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/get-token', [
    'uses' => 'TokenController@getToken'
]);
Route::get('/user/data', [
    'uses' => 'TokenController@getUserData'
]);
Route::post('/check-token', [
    'uses' => 'TokenController@checkToken'
]);
Route::post('/register', [
    'uses' => 'Auth\AuthController@register'
]);
Route::get('/list/genres', [
    'uses' => 'GenreController@getlistGenres'
]);
Route::post('/create-genre', [
    'uses' => 'GenreController@create'
]);
Route::get('/genre', [
  'uses' => 'GenreController@getGenre'
]);
Route::get('/list/stations', [
    'uses' => 'StationController@getlistStations'
]);
Route::get('/lastadded/stations', [
    'uses' => 'StationController@getlistLastAddedStations'
]);
Route::post('/create-station', [
    'uses' => 'StationController@create'
]);
Route::post('/edit-station', [
    'uses' => 'StationController@edit'
]);
Route::post('/create-favorite', [
    'uses' => 'FavoritesController@create'
]);
Route::post('/remove-favorite', [
  'uses' => 'FavoritesController@remove'
]);
Route::get('/list/favorites', [
  'uses' => 'FavoritesController@getListFavorites'
]);
Route::post('/create-language', [
    'uses' => 'LanguageController@create'
]);
Route::get('/detail-station', [
    'uses' => 'StationController@getDetail'
]);
Route::get('/get-logo', [
  'uses' => 'StationController@getLogoStation'
]);