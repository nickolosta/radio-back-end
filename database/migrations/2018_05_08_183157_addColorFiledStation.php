<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColorFiledStation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('stations', function($table)
      {
        $table->string('background')->nullable()->default('#000');
        $table->string('labelcolor')->nullable()->default('#fff');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('stations', function($table)
      {
        $table->dropColumn('background');
        $table->dropColumn('labelcolor');
      });
    }
}
