<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsStations2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stations', function($table)
        {
            $table->string('place')->nullable()->change();
            $table->text('contacts')->nullable()->change();
            $table->string('site')->nullable()->change();
            $table->string('email')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stations', function($table)
        {
            $table->string('place')->nullable(false)->change();
            $table->text('contacts')->nullable(false)->change();
            $table->string('site')->nullable(false)->change();
            $table->string('email')->nullable(false)->change();
        });
    }
}
