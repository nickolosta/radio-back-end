<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsStations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('stations', function($table)
        {
            $table->string('place');
            $table->integer('language_id')->unsigned()->nullable();
            $table->foreign('language_id')->references('id')->on('languages');
            $table->text('contacts');
            $table->string('site');
            $table->string('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('stations', function($table)
        {
            $table->dropColumn('place');
            $table->dropColumn('language');
            $table->dropColumn('contacts');
            $table->dropColumn('site');
            $table->dropColumn('email');
        });
    }
}
