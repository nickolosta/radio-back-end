<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableStationsGenres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stations_genres', function (Blueprint $table) {
            $table->integer('genre_id')->unsigned()->nullable();
            $table->foreign('genre_id')->references('id')
                ->on('genres')->onDelete('cascade');

            $table->bigInteger('station_id')->unsigned()->nullable();
            $table->foreign('station_id')->references('id')
                ->on('stations')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stations_genres');
    }
}
