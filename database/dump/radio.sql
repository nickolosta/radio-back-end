-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Oct 02, 2018 at 06:32 PM
-- Server version: 5.7.22-0ubuntu0.17.10.1
-- PHP Version: 7.1.19-1+ubuntu17.10.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `radio`
--

-- --------------------------------------------------------

--
-- Table structure for table `confirm_registration`
--

CREATE TABLE `confirm_registration` (
  `id` int(10) UNSIGNED NOT NULL,
  `last_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `hash` text COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `confirm_registration`
--

INSERT INTO `confirm_registration` (`id`, `last_name`, `name`, `password`, `email`, `created_at`, `updated_at`, `hash`) VALUES
(1, 'Степанов', 'Николай', '$2y$10$by4KkRgAG5zqfxzwWlKxeOUzMitX1dG.tDeTBfsMDE/dd8A6MdkOa', 'nickorsk2017@gmail.com', '2017-10-22 12:43:14', NULL, 'oal8un0KW9KjKIzbgGvh'),
(2, 'Stepanov', 'Nick', '$2y$10$hxIGUKtAyhqEvd2N0iMcC.UQKGWsGNXa8ra2H1hwIj.Ro.0TqfUlG', 'nickorsk2017@gmail.com', '2017-11-07 14:39:55', NULL, 'PRD5zqaWBLKnwMHRRWNn'),
(3, 'Stepanov', 'Nick', '$2y$10$lq.Ljc11x..GNIOgsigQiuICsJaV5o46NgKA1Op.7vAlSLqJ2RK0K', 'nickorsk2017@gmail.com', '2017-11-07 14:42:00', NULL, 'dT64AtxoNokw8ACHNToG'),
(4, 'Stepanov', 'Nick', '$2y$10$FGD9XO3BrLzli8b0OwCtTuX2yWbhnwKWOC6XZataVzTlczbO12mzq', 'nickorsk2017@gmail.com', '2017-11-07 14:44:12', NULL, 'FeUppwO6rw6CcRZUGyk5'),
(5, 'Stepanov', 'Nick', '$2y$10$7yzpnPUOBt4VgO8w3LbJKedZXS5c/cSrnN7znLTH5YWlrxqlilWt.', 'nickorsk2017@gmail.com', '2017-11-07 14:44:41', NULL, '2k6gkeiCos0MBqYzYXvr'),
(6, 'Stepanov', 'Nick', '$2y$10$tZ8wMiY./qZg0PcFNAaxse.gLkJR4msx5YxZ5NwkbzUvrOq9NMeXa', 'nickorsk2017@gmail.com', '2017-11-07 14:44:56', NULL, 'swxUJhYCcE1wI1r2S1NG'),
(7, 'Stepanov', 'Nick', '$2y$10$PRim1Bwvd3/WzIyBOe4ntenRhljjWWRqt26yWQHFzi7kt7s4r07Pu', 'nickorsk2017@gmail.com', '2017-11-07 14:45:23', NULL, 'bWvbdu4awSUVAaqC2dbS'),
(8, 'Stepanov', 'Nick', '$2y$10$axUz0YYrgouafPzXLTU6CuenlIojfOVQZjmUWajWMo0tcRCWZiw3.', 'nickorsk2017@gmail.com', '2017-11-07 14:45:34', NULL, 'BTYk0n7JiVK2N0rxFDtX'),
(9, 'Stepanov', 'Nick', '$2y$10$VkGG4h3YEDa9xGxToNKlhOXIIU.AbdUGKqipjb3rrdNyfOoVTy14S', 'nickorsk2017@gmail.com', '2017-11-07 14:45:45', NULL, 'X6yUaze0BLnNNeWMk5sG'),
(10, 'Stepanov', 'Nick', '$2y$10$dJy3C04iyvQwpGDa7xpTe..iVeF3dco6GL6RmQf8ao1zzPXpj2HEu', 'nickorsk2017@gmail.com', '2017-11-07 14:46:07', NULL, 'DCWnHmBZGArrbMh6q3Zz'),
(11, 'Stepanov', 'Nick', '$2y$10$AYcB6EehGFJ9chH.W7X9m.h8kHbzir.vYa0dHf1IpnjyQiUOvIwya', 'nickorsk2017@gmail.com', '2017-11-07 14:46:16', NULL, 'GGtILgZQWpnK8TLyDvRH'),
(12, 'Stepanov', 'Nick', '$2y$10$6YjHZ4xd9rlSBeyi4/SVd.iiO1Z8eNo24wXwlKeCIZPGyYmlReh.6', 'nickorsk2017@gmail.com', '2018-05-16 12:06:04', NULL, 'DAZPN6z5TMVnNjOqDQGj'),
(13, 'Stepanov', 'Nick', '$2y$10$2sGYpxe7wywlAMuIehLUbuo3vxJK5hZTVXFyZLr/fxyJvSY62nMLW', 'nickorsk2017@gmail.com', '2018-05-17 09:56:14', NULL, 'oXvYm81jXIJTaX8zcJcD'),
(14, 'Stepanov', 'Nick', '$2y$10$0Bh6myhWbZr6V1t5pnIXveM8CcxuSKpA0RcSsoBx82CgTBIHcPbwm', 'nickorsk2017@gmail.com', '2018-05-17 09:56:22', NULL, 'G6bcPiIznrDYYLUTiKA9'),
(15, 'Stepanov', 'Nick', '$2y$10$VP66LSy/ZruYwBh7thL6pOwk522CydB2NBVnqaGu.ZiSld22fFXuC', 'nickorsk2017@gmail.com', '2018-05-17 09:56:44', NULL, 'QAhhKGNaVntCD1tGONzI'),
(16, 'Stepanov', 'Nick', '$2y$10$gmKx8G6lMLvhaft5ajexC.PclCV54HbbUXJjOyxFbiKu6JFjjytlC', 'nickorsk2017@gmail.com', '2018-05-17 09:57:24', NULL, '2OKacAnmkDSvpdOXTDHC'),
(17, 'Stepanov', 'Nick', '$2y$10$eNUo4U.pVXktzEs4LEARTeA3ZYOq/rVYSmXZ9wYlnkBIGchPuV7W.', 'nickorsk2017@gmail.com', '2018-05-17 09:57:57', NULL, 'SzhneJTm78d73sVUHJxf'),
(18, 'Stepanov', 'Nick', '$2y$10$zSxq9w0yZHmIx1cFJy/.JupsakxT5DvMoy9ZDBgofasctKQTU5LTG', 'nickorsk2017@gmail.com', '2018-05-17 09:59:55', NULL, 'MlBL9CZEGzJITNNeanoV'),
(19, 'Stepanov', 'Nick', '$2y$10$LL/l9rBKbRPPF6OOvrbnpe1gLv60kpIrnZNFEsq4vRwUUoZ0Td4eq', 'nickorsk2017@gmail.com', '2018-05-17 10:00:12', NULL, 'wRRXOeENrHFCjYKCoeAL'),
(20, 'Stepanov', 'Nick', '$2y$10$dLX8U9S3ZCNfPiDxIrQXDO73D3LDiK.fcxQ/mX8ZiMLe3qVj9BNC.', 'nickorsk2017@gmail.com', '2018-05-17 10:00:40', NULL, 'G70HeyTjW0QyTW6RNP3t'),
(21, 'Stepanov', 'Nick', '$2y$10$xcy1KIoClOaQ/NNkezi8lOEoQxPh.VbelY8wcgBjF1EvFkSEsgQEm', 'nickorsk2017@gmail.com', '2018-05-17 10:04:10', NULL, 'hI8UpEL5gLK0Rjx9hWsG'),
(22, 'Stepanov', 'Nick', '$2y$10$McH3jITTOBUy4A9jaMZaZuBH7b5lRbL7veXZG5m4kY9FgS7VQYami', 'nickorsk2035@gmail.com', '2018-06-29 11:18:26', NULL, 'DOBwppH85wBJBs2INCPt'),
(23, 'Stepanov', 'Nick', '$2y$10$vNEkOBh439Zoe0EGrn.TI./gHlEi/h7lR2MA2XK5FZxmiCPK0SF/G', 'nickorsk2035@gmail.com', '2018-06-29 11:18:57', NULL, 'pugB3RFZQFNS2bvNt5pQ'),
(24, 'Stepanov', 'Nick', '$2y$10$SsJKVAVNOfS8K3r4Pec7hOhGYsnMUJxBccgAr0Mdi/vyZnlgj8uXC', 'nickorsk2035@gmail.com', '2018-06-29 11:19:24', NULL, 'w0sLycxVikE2rVNM9bDj'),
(25, 'Stepanov', 'Nick', '$2y$10$6V1EP1KSUjVgDki.Y2safOutOCkGvefE4IIo5ubMp3jtuS4neVs32', 'nickorsk2035@gmail.com', '2018-06-29 11:21:03', NULL, 'AISECAlPNoibpK5PnSV5'),
(26, 'Stepanov', 'Nick', '$2y$10$RQ/MuJtDueqqNedjsJrHputlqI91JfRHXsvYQEXJH9WPEWZqxX1Qi', 'nickorsk2035@gmail.com', '2018-06-29 11:22:37', NULL, 'deKPmJzpgveqZVQ1rTG4');

-- --------------------------------------------------------

--
-- Table structure for table `favorites`
--

CREATE TABLE `favorites` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `station_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `favorites`
--

INSERT INTO `favorites` (`id`, `station_id`, `user_id`, `created_at`, `updated_at`) VALUES
(13, 51, 2, '2018-08-03 12:26:33', '2018-08-03 12:26:33'),
(16, 44, 2, '2018-08-03 13:00:42', '2018-08-03 13:00:42'),
(17, 53, 2, '2018-08-03 13:08:27', '2018-08-03 13:08:27');

-- --------------------------------------------------------

--
-- Table structure for table `genres`
--

CREATE TABLE `genres` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `parentId` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `hasChild` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `genres`
--

INSERT INTO `genres` (`id`, `name`, `parentId`, `created_at`, `updated_at`, `hasChild`) VALUES
(1, '60', 0, '2017-09-27 12:40:23', '2017-09-27 12:40:23', 0),
(2, '70', 0, '2017-09-27 12:40:33', '2017-09-27 12:40:33', 0),
(3, '80', 0, '2017-09-27 12:40:37', '2017-09-27 12:40:37', 0),
(4, '90', 0, '2017-09-27 12:40:40', '2017-09-27 12:40:40', 0),
(5, 'Blues', 0, '2017-09-27 12:42:40', '2017-09-27 12:42:40', 0),
(6, 'Bollywood', 0, '2017-09-27 12:43:49', '2017-09-27 12:43:49', 0),
(7, 'Children_s_music', 0, '2017-09-27 12:45:22', '2017-09-27 12:45:22', 0),
(8, 'Jazz', 0, '2017-09-27 12:46:13', '2017-09-27 12:46:13', 0),
(9, 'Disco', 0, '2017-09-27 12:48:44', '2017-09-27 12:48:44', 0),
(10, 'Indie_music', 0, '2017-09-27 12:50:56', '2017-09-27 12:50:56', 0),
(11, 'Country_Music', 0, '2017-09-27 12:51:37', '2017-09-27 12:51:37', 0),
(12, 'Classical_music', 0, '2017-09-27 12:52:29', '2017-09-27 12:52:29', 0),
(13, 'Latin_America_music', 0, '2017-09-27 12:54:03', '2017-09-27 12:54:03', 0),
(14, 'New_age', 0, '2017-09-27 12:54:56', '2017-09-27 12:54:56', 0),
(15, 'Pop_music', 0, '2017-09-27 12:55:20', '2017-09-27 12:55:20', 0),
(16, 'Reggaeton', 0, '2017-09-27 12:55:45', '2017-09-27 12:55:45', 0),
(17, 'Reggae', 0, '2017-09-27 12:56:26', '2017-09-27 12:56:26', 0),
(18, 'Religion', 0, '2017-09-27 12:56:58', '2017-09-27 12:56:58', 0),
(19, 'Retro', 0, '2017-09-27 12:57:16', '2017-09-27 12:57:16', 0),
(20, 'R_and_B', 0, '2017-09-27 12:58:00', '2017-09-27 12:58:00', 0),
(21, 'Rock', 0, '2017-09-27 12:58:09', '2017-09-27 12:58:09', 0),
(22, 'Funk', 0, '2017-09-27 12:58:30', '2017-09-27 12:58:30', 0),
(23, 'Folk', 0, '2017-09-27 12:59:01', '2017-09-27 12:59:01', 0),
(24, 'HipHop', 0, '2017-09-27 12:59:14', '2017-09-27 12:59:14', 0),
(25, 'Electronic_music', 0, '2017-09-27 12:59:52', '2017-09-27 12:59:52', 0),
(26, 'Ambient', 0, '2017-09-27 13:01:20', '2017-09-27 13:01:20', 0),
(27, 'Alternative_rock', 21, '2017-09-29 10:52:57', '2017-09-29 10:52:57', 0),
(28, 'Classic_rock', 21, '2017-10-09 14:47:26', '2017-10-09 14:47:26', 0),
(29, 'Community', 0, '2017-11-09 02:59:56', '2017-11-09 02:59:56', 0),
(30, 'Christian_rock', 21, '2017-11-09 04:32:04', '2017-11-09 04:32:04', 0),
(31, 'Sports_news', 0, '2017-11-09 04:43:47', '2017-11-09 04:43:47', 0);

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE `languages` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Russian', '2017-10-29 14:13:04', '2017-10-29 14:13:04'),
(2, 'English', '2017-11-07 12:09:07', '2017-11-07 12:09:07'),
(3, 'Spanish', '2017-11-16 10:42:45', '2017-11-16 10:42:45');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_07_12_190111_genres', 1),
(4, '2017_07_12_190244_stations', 1),
(5, '2017_07_18_145512_token', 1),
(6, '2017_07_18_151905_confirm_registration', 1),
(7, '2017_07_31_150722_favorite', 1),
(8, '2017_09_10_081837_addHasChildGenres', 1),
(9, '2017_09_29_163149_addField_URL_Logo_for_stations', 2),
(10, '2017_10_01_093122_setDescriptionIsNotRequered_stations', 3),
(15, '2017_10_29_165314_Languages', 4),
(16, '2017_10_29_174728_addFieldsStations', 5),
(17, '2017_11_07_175435_removeStationRelation', 6),
(18, '2017_11_07_180017_addTable_stations_genres', 7),
(19, '2017_11_09_074630_addFieldsStations_2', 8),
(20, '2018_05_08_173042_changeStationURL_LogoRequered', 9),
(22, '2018_05_08_183157_addColorFiledStation', 10);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stations`
--

CREATE TABLE `stations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `logoFileInfo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `urlSound` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `countView` bigint(20) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `URL_logo` text COLLATE utf8mb4_unicode_ci,
  `place` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `language_id` int(10) UNSIGNED DEFAULT NULL,
  `contacts` text COLLATE utf8mb4_unicode_ci,
  `site` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `background` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '#000',
  `labelcolor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT '#fff'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stations`
--

INSERT INTO `stations` (`id`, `name`, `description`, `logoFileInfo`, `urlSound`, `countView`, `created_at`, `updated_at`, `URL_logo`, `place`, `language_id`, `contacts`, `site`, `email`, `background`, `labelcolor`) VALUES
(44, 'Rock 102', 'We bring you the best new rock, the best classic rock and the best concerts!', '', 'http://rawlco.leanstream.co/CJDJFM', 0, '2017-11-07 13:56:10', '2018-05-08 11:35:27', 'http://media.socastsrm.com/uploads/station/552/site_header_logo-585ab033a5eda.png', 'Canada, Saskatoon', 2, '715 Sask Crescent West Saskatoon, Sk S7M 5V7 (306) 938-3733', 'http://www.rock102rocks.com/', 'rzimmerman@rawlco.com', '#000', '#fff'),
(45, 'Project 88.7', 'On the surface Project:88-7 is a radio station playing great music from artists like LeCrae, Andy Mineo, and Family Force 5 that won’t de-humanize anyone. Behind those frequency numbers is a heart beating with a passion to bring people together in order to impact the world', '', 'http://50.22.253.46/koay-fm.mp3', 0, '2017-11-09 02:49:28', '2017-11-09 02:49:28', 'http://www.project887.org/images/logo.png', 'USA,ID,Boise', 2, '208-455-0887', 'http://www.project887.org/', NULL, '#000', '#fff'),
(46, 'Radio Boise', 'Radio Boise is a community radio station. Our volunteer programmers strive to provide quality local programming for the wide range of people and communities in our listening area who are not satisfied by existing media outlets.', '', 'http://radioboise-ice.streamguys1.com/live', 0, '2017-11-09 03:01:51', '2017-11-09 03:01:51', 'https://dqa4a6x5zonsi.cloudfront.net/img/stations/radioboise/RadioBoise-Logo.png', 'USA,ID,Boise', 2, 'Radio Boise, 1020 West Main Street, Suite #50, Boise, ID, 83702', 'https://radioboise.us/', 'info@radioboise.org', '#000', '#fff'),
(47, 'Boise State Public Radio (Classical)', 'Boise State Public Radio is southern and central Idaho\'s nonprofit, listener-supported NPR member station, reaching the region\'s metropolitan and rural areas.\r\n\r\nAs the only sources of quality public radio news, classical music, jazz and unique cultural programming, our radio stations are distinct in their mission, their audience and their partnerships.', '', 'https://18543.live.streamtheworld.com/KBSUFM.mp3', 0, '2017-11-09 03:12:05', '2017-11-09 03:12:05', 'http://mediad.publicbroadcasting.net/p/idaho/files/201502/boise-logo-204.png', 'USA,ID,Boise', 2, 'Mailing Address: 1910 University Dr., Boise, ID 83725-1916    Physical Address: Yanke Family Research Park, 220 E Parkcenter Blvd., Boise, ID 83706 (Take an immediate left turn after entering the first set of doors.)  Phone: (208) 426-3663', 'http://boisestatepublicradio.org', 'boisestatepublicradio@boisestate.edu', '#fff', '#000'),
(48, 'Boise State Public Radio (Jazz)', 'Boise State Public Radio is southern and central Idaho\'s nonprofit, listener-supported NPR member station, reaching the region\'s metropolitan and rural areas. As the only sources of quality public radio news, classical music, jazz and unique cultural programming, our radio stations are distinct in their mission, their audience and their partnerships.', '', 'http://66.193.105.58:8000/stream', 0, '2017-11-09 03:18:08', '2017-11-09 03:18:08', 'http://mediad.publicbroadcasting.net/p/idaho/files/201502/boise-logo-204.png', 'USA,ID,Boise', 2, '1910 UNIVERSITY DRIVE  Boise, ID 83725  (208) 426-3663', 'http://boisestatepublicradio.org', 'boisestatepublicradio@boisestate.edu', '#fff', '#000'),
(49, 'Radio Nueva Vida (Ispan christian music)', 'Musica, programacion para toda la familia, las 24 horas del dia.', '', 'http://ic1.christiannetcast.com/nuevavida', 0, '2017-11-09 03:23:19', '2017-11-09 03:23:19', 'http://nuevavida.com/wp-content/uploads/2016/07/RNVLogo-menu2-1.png', 'USA,ID,Boise', 2, 'PO BOX 500, Camarillo, CA 93010, USA  1-800-260-5676', 'http://www.nuevavida.com/', 'info@nuevavida.com', '#fff', '#000'),
(50, 'Effect Radio', 'Our goal is to share the gospel with the lost, and to provide the Christian community with best modern Christian music available today.', '', 'http://ice6.securenetsystems.net/EFXAAC?playSessionID=41136145-155D-C0F3-032AD812C41BC8B7', 0, '2017-11-09 04:32:28', '2017-11-09 04:32:28', 'http://effectradio.com/img/logoHeader.svg', 'USA,ID,Boise', 2, 'PO Box 271  Twin Falls ID 83303  208.734.2049', 'http://www.effectradio.com/', NULL, '#c75454', '#fff'),
(51, 'Kissin\' 92', 'KIZN is Boise’s “Big Market” Country station, reflecting the lifestyle of the contemporary Country fan in the Treasure Valley. KIZN is the top rated radio country station in the Boise market with Adults 25-54 (Fall 2008 Arbitron – Boise Metro). Kissin’ 92.3 is the most consistent radio performer in the market, consistently ranking in the Top 5 for nearly every demographic. KIZN is an official CMT affiliate and features interviews with Country’s biggest stars. A family station with no blue or questionable content or music, Kissin’ 92.3 is very active with local promotions and giveaway contests.', '', 'https://19273.live.streamtheworld.com/KIZNFMAAC.aac?postalcode=24042&yob=1985&gender=m&dist=tg&tdsdk=js-2.9&pname=TDSdk&pversion=2.9&banners=300x250&sbmid=4aaaace0-1b6b-4fa2-8d99-ab5c35bed06d', 0, '2017-11-09 04:40:41', '2017-11-09 04:40:41', 'https://d1os6sc1n72spf.cloudfront.net/brandassets/KIZN-FM-TuneGenie.png', 'USA,ID,Boise', 2, 'KIZN – Kissin’ 92.3 1419 West Bannock Boise, ID 83702, Business Phone: 208-336-3670', 'http://www.kizn.com/', 'kissin92@kizn.com', '#000', '#fff'),
(52, '93.1 KTIK', '93.1 FM KTIK “The Ticket” is Boise’s exclusive 24- hour sports-talk station. 1350 KTIK is the voice of Idaho Steelheads Hockey. “Idaho Sports Talk with Caves and Prater” dominates the local Male sports audience. •KTIK Sports Update •The Scott Slant w/ Tom Scott •KTIK Traffic & Weather Reports •Idaho Steelheads Hockey (am only)•Featured NCAA Football •NFL Games Other Programming & Features In The Community As the top sports radio station for the Treasure Valley, KTIK is involved with many events like the Boise State football women’s football clinic, Hero Care, Toys for Tots, & Kids’ Fair.', '', 'https://18583.live.streamtheworld.com/KTIKFMAAC.aac?postalcode=24042&yob=1985&gender=m&dist=tg&tdsdk=js-2.9&pname=TDSdk&pversion=2.9&banners=300x250&sbmid=0e91970e-a585-41a0-9ab3-a7048a85e426', 0, '2017-11-09 04:47:35', '2017-11-09 04:47:35', 'http://cumulus.pro.poolb.tritondigitalcms.com/ktik-af/wp-content/uploads/sites/317/2015/04/931-KTIK-2013-Final-e1428608478754.png', 'USA,ID,Boise', 2, '1419 W. Bannock Street  Boise, ID  83702  US    208.336.3670', 'http://www.ktik.com/', 'andrew.paul@citcomm.com', '#fff', '#000'),
(53, '94.1 The voice', 'Lee and Beth Schafer founded Inspirational Family Radio in May, 1983. Lee’s interest in radio and electronics began early in his life. He built his first radio at age seven, and earned his amateur radio license when he was ten years old. He pursued his love and learning of radio by achieving his First Class License while in college.', '', 'http://ic2.christiannetcast.com/kbxl-fm?rnd=381', 0, '2017-11-09 04:56:03', '2017-11-09 04:56:03', 'https://i2.wp.com/www.941thevoice.com/wp-content/uploads/2017/09/KBXL_LOGO_white.png?w=807', 'USA,ID,Boise', 2, '1440 S Weideman Ave, Boise, ID 83709 1-208-377-3790', 'http://www.941thevoice.com/', 'info@myfamilyradio.com', '#33778b', '#fff'),
(54, 'The River 94.9', 'The fact that 94.9 the River is challenging to describe in a sentence (or a page, for that matter) is exactly what makes it appealing to an adult audience who prefers genuine diversity in music - and \"genuineness\" in everything else. From CSN to R.E.M., Bob Marley to Dave Matthews, the River touches on a broad spectrum of music, appealing to a broad range of tastes. In addition, we\'ve made a significant commitment to \"community,\" building an enviable list of alliances and reaching an equally enviable audience in terms of education, income, and other qualitative measures.\r\n \r\n 94.9 the River is ... World Class Rock for southern Idaho & the Northwest.', '', 'https://14623.live.streamtheworld.com/KRVBFMAAC.aac?dist=tg&tdsdk=js-2.9&pname=TDSdk&pversion=2.9&banners=300x250&sbmid=f33ed6ea-ede8-4d5d-fa4c-535e69fcb060', 0, '2017-11-09 05:09:48', '2017-11-09 05:09:48', 'http://assets.scrippsdigital.com/cms/images/color_schemes/krvb/full--100.png', 'USA,ID,Boise', 2, '5257 Fairview Ave., Ste. 240 Boise, ID 83706, Office number: (208) 344-3511', 'http://www.riverinteractive.com/', NULL, '#fff', '#000'),
(55, 'CSN 95.7', NULL, '', 'http://ice7.securenetsystems.net/CSNAAC?playSessionID=42B295CA-155D-C0F3-0355B96183955A2B', 0, '2017-11-09 05:16:05', '2017-11-09 05:16:05', 'http://csnradio.com/img/headerLogo.svg', 'USA,ID,Boise', 2, 'PO Box 391 Twin Falls, ID 83303, 800.357.4226 - Toll Free', 'http://csnradio.com', NULL, '#c75454', '#fff'),
(56, '96.9 The Eagle', '96.9 The Eagle\r\n(KKGL-FM) is Boise’s Classic Rock Station playin the best rock of the 70’s and 80’s, a few 90’s as well as classics from the 60’s.', '', 'https://16693.live.streamtheworld.com/KKGLFMAAC.aac?postalcode=19966&yob=1985&gender=m&dist=tg&tdsdk=js-2.9&pname=TDSdk&pversion=2.9&banners=300x250&sbmid=1fa4d2bd-f3be-45e6-9c9b-c6db1096dde5', 0, '2017-11-09 05:36:34', '2017-11-09 05:36:34', 'http://cumulus.pro.poolb.tritondigitalcms.com/kkgl-fm/wp-content/uploads/sites/311/2015/03/site-logo.png', 'USA,ID,Boise', 2, '1419 W. Bannock Street  Boise, ID  83702  US    208.336.3670', 'http://www.96-9theeagle.com/', 'nick.coe@cumulus.com', '#33778b', '#fff'),
(57, 'Nash FM 97.9', NULL, '', 'https://20263.live.streamtheworld.com/WXTAFMAAC.aac?postalcode=10012&yob=1985&gender=m&dist=tg&tdsdk=js-2.9&pname=TDSdk&pversion=2.9&banners=300x250&sbmid=778e270c-06ca-479a-8722-744d07a3d3a2', 0, '2017-11-09 05:46:51', '2017-11-09 05:46:51', 'none', 'USA,ID,Boise', 2, '1419 W. Bannock Street  Boise, ID  83702  US    208.336.3670', 'http://www.979nashfm.com/', NULL, '#fff', '#000'),
(58, '99.1 La Perrona', 'La casa de Don Cheto al Aire, El Méndigo Chupacabras, Erazno y La Chokolata, La Chaparrita Ramírez y Francisco \"El Espiderman\".', '', 'http://ic2.mainstreamnetwork.com/kxta-fm?rnd=9878', 0, '2017-11-16 10:46:28', '2017-11-16 10:46:28', 'http://www.mainstreamnetwork.com/listen/images/kxta_logo.png', 'USA,ID,Boise', 3, '208-324-8181', 'http://991laperrona.com/', NULL, '#fff', '#000'),
(59, '88.5 FM So Cal', '88.5 FM is Southern California\'s only non-commercial station providing an eclectic blend of Modern Rock, great rock legends, Americana, and singer-songwriters.', '', 'http://130.166.82.184:8000/;stream.mp3', 0, '2018-05-08 11:49:30', '2018-05-08 11:49:30', '', 'USA,Northridge', 2, 'KCSN-FM  18111 Nordhoff Street  Northridge, CA 91330-8312  818-677-3090', 'http://www.885fm.org/', 'feedback@kcsn.org', '#c75454', '#fff'),
(60, '181.FM Chloe@181.FM', '181.FM Internet Radio - The Best Choice for Radio. Your Lifestyle, Your Music', '', 'http://listen.181fm.com/181-chloe_128k.mp3', 0, '2018-05-08 12:04:09', '2018-05-08 12:04:09', 'http://player.181fm.com/configs/images/181fm.png', 'USA', 2, 'PO Box 927 Waynesboro, Virginia 22980 USA  540-908-4332', 'http://www.181.fm/', 'contact@181.fm', '#fff', '#000'),
(61, 'LiVE 88.5', 'Ottawa\'s Alternative Rock - LiVE 88.5 - is Canada\'s first and only carbon neutral radio station.', '', 'http://newcap.leanstream.co/CILVFM', 0, '2018-05-08 12:13:46', '2018-05-08 12:13:46', 'http://media.socastsrm.com/uploads/station/834/site_header_logo-5909303754d14.png', 'Canada, Ottawa', 2, 'Newcap Radio  6 Antares Drive  Phase 1 Unit 100  Ottawa, Ontario  K2E 8A9  613.727.8850', 'http://www.live885.com/', 'sbroderick@newcap.ca', '#fff', '#000'),
(62, 'WCLZ', 'WCLZ 98.9 broadcasts the best rock and modern rock.', '', 'http://20593.live.streamtheworld.com/WCLZFMAAC.aac', 0, '2018-05-08 12:19:52', '2018-05-08 12:19:52', 'http://989wclz.com/wp-content/themes/wclz/img/logo.png', 'USA,ME,Yarmouth', 2, '420 Western Ave.  South Portland, ME   04106  207-871-9259', 'http://989wclz.com/', 'ethan@989wclz.com', '#fff', '#000'),
(63, '.977 Alternative', 'Modern Rock with an Edge. If you\'re looking for the largest library of current rock and retro alternative music, this is the channel for you. We play everything from 311 to Rob Zombie on this channel. Tune in and crank it up!', '{\"storageFolder\":\"2018_2q\",\"name\":\"2018_2q\",\"path\":\"2018_2q\\/GnJj8O5uq481ZMPS1ADRSCqmDb28xNz4yciPWa8G.png\",\"file_size\":28439,\"realFileName\":\"logoStation.png\",\"file_mime\":\"image\\/png\"}', 'http://19763.live.streamtheworld.com:3690/977_ALTERN_SC', 0, '2018-05-08 12:32:10', '2018-05-08 12:41:56', NULL, 'USA', 2, '3412 Lake Shore Cove Chaska MN 55318 US +1 954 905 5977', 'http://www.977music.com/', 'jeff@977music.com', '#fff', '#000');

-- --------------------------------------------------------

--
-- Table structure for table `stations_genres`
--

CREATE TABLE `stations_genres` (
  `genre_id` int(10) UNSIGNED DEFAULT NULL,
  `station_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stations_genres`
--

INSERT INTO `stations_genres` (`genre_id`, `station_id`, `created_at`, `updated_at`) VALUES
(15, 45, '2017-11-09 02:49:28', '2017-11-09 02:49:28'),
(24, 45, '2017-11-09 02:49:28', '2017-11-09 02:49:28'),
(18, 45, '2017-11-09 02:49:28', '2017-11-09 02:49:28'),
(29, 46, '2017-11-09 03:01:51', '2017-11-09 03:01:51'),
(12, 47, '2017-11-09 03:12:05', '2017-11-09 03:12:05'),
(8, 48, '2017-11-09 03:18:08', '2017-11-09 03:18:08'),
(18, 49, '2017-11-09 03:23:19', '2017-11-09 03:23:19'),
(30, 50, '2017-11-09 04:32:28', '2017-11-09 04:32:28'),
(11, 51, '2017-11-09 04:40:41', '2017-11-09 04:40:41'),
(31, 52, '2017-11-09 04:47:35', '2017-11-09 04:47:35'),
(18, 53, '2017-11-09 04:56:03', '2017-11-09 04:56:03'),
(21, 54, '2017-11-09 05:09:48', '2017-11-09 05:09:48'),
(18, 55, '2017-11-09 05:16:05', '2017-11-09 05:16:05'),
(28, 56, '2017-11-09 05:36:34', '2017-11-09 05:36:34'),
(15, 57, '2017-11-09 05:46:51', '2017-11-09 05:46:51'),
(13, 58, '2017-11-16 10:46:28', '2017-11-16 10:46:28'),
(21, 44, '2018-05-08 11:39:18', '2018-05-08 11:39:18'),
(20, 59, '2018-05-08 11:49:30', '2018-05-08 11:49:30'),
(27, 59, '2018-05-08 11:49:30', '2018-05-08 11:49:30'),
(27, 60, '2018-05-08 12:04:09', '2018-05-08 12:04:09'),
(27, 61, '2018-05-08 12:13:47', '2018-05-08 12:13:47'),
(21, 62, '2018-05-08 12:19:52', '2018-05-08 12:19:52'),
(27, 62, '2018-05-08 12:19:52', '2018-05-08 12:19:52'),
(21, 63, '2018-05-08 12:32:11', '2018-05-08 12:32:11'),
(27, 63, '2018-05-08 12:32:11', '2018-05-08 12:32:11');

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `api_token` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `token`
--

INSERT INTO `token` (`id`, `user_id`, `api_token`, `created_at`, `updated_at`) VALUES
(1, 1, 'AQgpk3gbG52oNUPusrH63NQMDVORkZlM1yoxPA42QOzIkAe7wPzsFH80gKuy', '2018-05-17 10:05:49', '2018-05-17 10:05:49'),
(19, 2, 'WdY1a28NAeC8uXicnjLJ3pLXaKQDe0Idiljyzjxk9sJWv2rw1llSlYSybZHV', '2018-08-03 13:02:20', '2018-08-03 13:02:20');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `last_name`, `password`, `active`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Nick', 'nickorsk2035@gmail.com', 'Stepanov', '$2y$10$UCHN8GdOXRpC5i3E58c6oOPN/hK0qqayU3wRN5gq6R/QYT6WFBzDO', 1, NULL, '2018-06-29 11:23:41', '2018-06-29 11:23:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `confirm_registration`
--
ALTER TABLE `confirm_registration`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `favorites`
--
ALTER TABLE `favorites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `favorites_station_id_foreign` (`station_id`),
  ADD KEY `favorites_user_id_foreign` (`user_id`);

--
-- Indexes for table `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `languages`
--
ALTER TABLE `languages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `stations`
--
ALTER TABLE `stations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stations_language_id_foreign` (`language_id`);

--
-- Indexes for table `stations_genres`
--
ALTER TABLE `stations_genres`
  ADD KEY `stations_genres_genre_id_foreign` (`genre_id`),
  ADD KEY `stations_genres_station_id_foreign` (`station_id`);

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `token_api_token_unique` (`api_token`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `confirm_registration`
--
ALTER TABLE `confirm_registration`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `favorites`
--
ALTER TABLE `favorites`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `genres`
--
ALTER TABLE `genres`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `languages`
--
ALTER TABLE `languages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `stations`
--
ALTER TABLE `stations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;
--
-- AUTO_INCREMENT for table `token`
--
ALTER TABLE `token`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `favorites`
--
ALTER TABLE `favorites`
  ADD CONSTRAINT `favorites_station_id_foreign` FOREIGN KEY (`station_id`) REFERENCES `stations` (`id`),
  ADD CONSTRAINT `favorites_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `stations`
--
ALTER TABLE `stations`
  ADD CONSTRAINT `stations_language_id_foreign` FOREIGN KEY (`language_id`) REFERENCES `languages` (`id`);

--
-- Constraints for table `stations_genres`
--
ALTER TABLE `stations_genres`
  ADD CONSTRAINT `stations_genres_genre_id_foreign` FOREIGN KEY (`genre_id`) REFERENCES `genres` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `stations_genres_station_id_foreign` FOREIGN KEY (`station_id`) REFERENCES `stations` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
