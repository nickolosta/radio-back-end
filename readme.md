Instruction (tested for Ubuntu):
1. install composer modules: 
php composer.phar install
2. settings:
create some data base from phpmyadmin
create .env file from .env.example (you don't must remove this file)
load dump to your data base from folder database/dump/radio.sql
3. run your server from root folder:
php artisan serve
4. check server is started (you can open api console):
http://localhost:8000/api/console/index