/**
 * Created by nick on 25.05.16.
 */
jQuery(function($){
    $(document).ready(function(){
        var Post = app_console.Post = {
            seletedDataIndex: 0,
            files:[],
            files_helpers:{
                findByName:function(name){
                    var files = Post.files;
                    for(var f=0;f<files.length;f++){
                        if(files[f].name==name){
                            return files[f];
                        }
                    }
                }
            },
            // рендер инпутов
            render_inputs:function (inputs,index) {
                var $container =$('#post_fields');
                var html ='';
                var input_type = 'text';
                var is_optional = false;
                var input_name = '';
                var require = true;
                var data = app_console.data.post[index];
                Post.seletedDataIndex = index;
                if(typeof data.title!='undefined'){
                    if(typeof data.desc!='undefined'){
                        html+='<p>'+data.desc+'</p>';
                    }
                }
                for(var d=0;d<inputs.length;d++){
                    input_type ='text';
                    is_optional = false;
                    input_name = inputs[d];
                    require = true;
                    if(inputs[d].indexOf('@password')>-1){
                        input_type ='password';
                        input_name = inputs[d].replace('@password','')
                    }
                    if(inputs[d].indexOf('@file')>-1){
                        input_type ='file';
                        input_name = inputs[d].replace('@file','');

                        Post.files.push(
                            {
                                name:input_name,
                                $file:null,
                                file_data:null,
                                get_file:function () {
                                    this.$file = $('#post_fields input[name="'+this.name+'"]');
                                },
                                events:function(){
                                    var th = this;
                                    th.$file.change(function(){
                                        var $ob = $(this);
                                        var file = $ob[0].files[0];
                                        th.file_data = file;
                                    });

                                }
                            }
                        );

                    }
                    if(inputs[d].indexOf('!')>-1){
                        is_optional= true;
                        input_name = inputs[d].replace('!','');
                        require = false;
                    }
                    if(inputs[d].indexOf('@textarea')>-1){
                        input_name = inputs[d].replace('@textarea','');
                        html+=input_name+'<br> <textarea  data-require="'+require+'" data-index="'+index+'" class="post_inputs" name="'+input_name+'""></textarea>';
                    } else {
                        html+=input_name+'<br> <input  data-require="'+require+'" data-index="'+index+'" class="post_inputs" name="'+input_name+'" type="'+input_type+'">';
                    }

                    if(is_optional){
                        html+=' необяз.';
                    }
                    html+='<br><br>';
                }
                html+='<a id="post_send_data" data-index="'+index+'">Отправить</a>';
                $container.html(html);

                $('#post_fields input[type="file"]').each(function () {
                    var $ob =$(this);
                    var name = $ob.attr('name');
                    var fileObs = Post.files_helpers.findByName(name);
                    fileObs.get_file();
                    fileObs.events();
                });
                Post.events_input();
            },
            // валидация инпутов
            validate_input:function(){
                var valid = true;
                $('.post_inputs').each(function(){
                    var $ob = $(this);
                    if($ob.attr('data-require')==true){
                        if($ob.val().trim().length==0){
                            valid = false;
                        }
                    }
                });
                return valid;
            },
            clearInputs:function(){
                if(typeof app_console.data.post[Post.seletedDataIndex].clearData != 'undefined'){
                    if(!app_console.data.post[Post.seletedDataIndex].clearData){
                        return;
                    }
                }
                $('.post_inputs').each(function () {
                    var $ob = $(this);
                    $ob.val('');
                });
            },
            //события инпутов
            events_input:function(){
                $('#post_send_data').on('click',function(e){
                    e.stopPropagation();
                    $('#post_result').html('');
                    var data = {};
                    var $ob =$(this);
                    var index = $ob.attr('data-index');
                    var data_post = app_console.data.post[index];
                    var formData = null;
                    var fileObs;
                    if(typeof data_post.formData!='undefined'){
                        formData = new FormData();
                        $('.post_inputs').each(function () {
                            var $ob = $(this);
                            if($ob.attr('type')=='file'){
                                fileObs= Post.files_helpers.findByName($ob.attr('name'));
                                formData.append($ob.attr('name'),fileObs.file_data);
                            }else{
                                formData.append($ob.attr('name'),$ob.val());
                            }
                            if(typeof data_post.formData_modifier!='undefined'){
                                data_post.formData_modifier(formData);
                            }
                        });
                    }else{
                        $('.post_inputs').each(function () {
                            var $ob = $(this);
                            data[$ob.attr('name')] = $ob.val();
                        });
                    }

                    if (Post.validate_input()) {
                        var url = $('#URL_res').val();
                        // установим токены если есть
                        var token = $('#login-token').val();
                        var token_user_id = $('#login-token-user').val();
                        if(token.length>0 && token_user_id>0){
                            $.ajaxSetup({
                                headers: { 'login-token' : token,'login-token-user':token_user_id }
                            });
                        }

                        var success_callback = function(Data,status, xhr){
                            var ct = xhr.getResponseHeader("content-type") || "";
                            if (ct.indexOf('text/html') > -1) {
                                $('#post_result').html('<iframe>'+Data+'</iframe>');
                            }
                            if (ct.indexOf('json') > -1) {
                                $('#post_result').jsonViewer(Data);
                            }
                            if (ct.indexOf('image') > -1) {
                                $('#post_result').html('<img>');

                                $("#post_result img").attr("src",$('#URL_res').val());
                            }
                            if (ct.indexOf('application/zip') > -1) {
                                var win = window.open(url, '_blank');
                                if(win){
                                    //Browser has allowed it to be opened
                                    win.focus();
                                }else{
                                    //Broswer has blocked it
                                    alert('Разрешите всплывание окна');
                                }
                            }
                            if(typeof data_post.callback=='function'){
                                data_post.callback(Data);
                            }
                            Post.clearInputs();
                        };
                        if(typeof data_post.formData!='undefined'){
                            $.ajax({
                                method: "POST",
                                url: url,
                                data: formData,
                                processData: false,
                                contentType: false,
                                cache: false,
                                success: success_callback,
                                error:function (e) {
                                    console.log(e);
                                    alert('Ошибка запроса: '+e.statusText);
                                }
                            });
                        }else{

                                $.ajax({
                                    url:url,
                                    method: "POST",
                                    data:data,
                                    success:success_callback,
                                    error:function (e) {
                                        console.log(e);
                                        alert('Ошибка запроса:'+e.statusText);
                                    }
                                });
                        }
                    }
                });
                //ввод значений в инпуте
                $('.post_inputs').keydown(function(e){
                    e.stopPropagation();
                    var index = $(this).attr('data-index');
                    var pattern = app_console.data.post[index].url;
                    $('.post_inputs').each(function(){
                        var $ob = $(this);
                        var name = $ob.attr('name');
                        setTimeout(function () {
                            var val = $ob.val().trim();
                            if(val.length>0){
                                pattern = pattern.replace('{'+name+'}',val);
                                $('#URL_res').val(pattern);
                            }
                        },100);
                    });
                });
            },
            // рендер общий
            render:function(){
                var data = app_console.data.post;
                var html ='';
                var $container =$('#post_select_route');
                for(var d=0;d<data.length;d++){
                    if(typeof data[d].title!='undefined'){
                        html+='<option class="select_get" value="'+d+'">'+data[d].title+'</option>';
                    }else{
                        html+='<option class="select_get" value="'+d+'">'+data[d].desc+'</option>';
                    }
                }
                $container.html(html);
                Post.events();
            },
            //
            events:function(){
                (function(){
                    var data = app_console.data.post[0];
                    Post.render_inputs(data.fields,0);
                })();
                $('#post_select_route').change(function(e){
                    e.stopPropagation();
                    var $ob =$(this);
                    var index = parseInt($ob.val());
                    var data = app_console.data.post[index];
                    $('#URL_res').val(data.url);
                    Post.render_inputs(data.fields,index);
                });

            },
            init:function(){
                Post.render();
            }
        };
        Post.init();
    });
});