/**
 * Created by nick on 23.05.16.
 */
jQuery(function($){
    $(document).ready(function(){
        var data = app_console.data = {
            get:[
                /** list genres */
                {
                    title:'List genres',
                    desc:'List genres',
                    url:'/api/list/genres?genre_id={genre_id}',
                    fields: ['genre_id']
                },
                {
                    title:'List stations',
                    desc:'List stations',
                    url:'/api/list/stations?genre_id={genre_id}',
                    fields: ['genre_id']
                }
            ],
            post:[
                /** Get token  */
                {
                    desc:'Get token',
                    url:'/api/get-token',
                    fields: ['email','password@password'],
                    callback:function (Data) {
                        if(Data.success) {
                            var data = Data.data;
                            $('#login-token').val(data.token);
                            $('#login-token-user').val(data.id_user);
                        }
                    }
                },
                /** Получить токен OAUTH  */
                {
                    title:'авторизация или регистрация oauth',
                    desc:'происходит при деавторизованном пользователе (заголовки токена не нужно отсылать)' +
                    '<br> пользователь должен быть не авторизированным в браузере' +
                    '<br> <b>driver: </b> vkontakte, facebook',
                    url:'/outh-by-token',
                    fields: ['code','state','driver'],
                    callback:function (Data) {
                        if(Data.success) {
                            var data = Data.data;
                            $('#login-token').val(data.token);
                            $('#login-token-user').val(data.id_user);
                        }
                    }
                },
                /** Check token */
                {
                    desc:'Check token',
                    url:'/api/check-token',
                    fields: []
                },
                /** register user */
                {
                    desc:'Register user',
                    url:'/api/register',
                    fields: ['name','last_name','email','password@password','password_confirmation@password']
                },
                /** reset password */
                {
                    desc:'Reset password',
                    url:'/reset-password-ajax',
                    fields: ['email']
                },
                /** Create genre */
                {
                    desc:'Create genre',
                    url:'/api/create-genre',
                    fields: ['!name','!parentId'],
                    callback:function (Data) {
                        if(Data.success) {
                            var data = Data.data;

                        }
                    }
                },
                /** Create Station */
                {
                    desc:'Create station',
                    url:'/api/create-station',
                    fields: ['name','description@textarea','genres','logo@file','urlSound','countView', 'URL_logo', 'place', 'language_id', 'contacts', 'site', 'email'],
                    formData:true
                },
                /** Edit Station */
                {
                    desc:'Edit station',
                    url:'/api/edit-station',
                    fields: ['id','name','description@textarea','genres','logo@file','urlSound','countView', 'URL_logo', 'place', 'language_id', 'contacts', 'site', 'email'],
                    formData:true,
                    clearData: false
                },
                 /** Create favorite */
                {
                    desc:'Create favorite',
                    url:'/api/create-favorite',
                    fields: ['station_id','user_id']
                },
                /** Create language */
                {
                    desc:'Create language',
                    url:'/api/create-language',
                    fields: ['name']
                }
        ],
            socket:{
                connect:'http://'+document.domain+':3000',
                emit:[
                    {
                        title:'ставим свой онлайн статус в чате',
                        emit:'chat online',
                        params:['id_user','id_task'],
                        func:function(){
                            app_console.socket.socket.emit('chat online',$('#socket_inp_id_user').val(),$('#socket_inp_id_task').val());
                        }
                    },
                    {
                        title:'получаем данные чата',
                        emit:'chat get',
                        params:['id_user','id_task'],
                        func:function(){
                            app_console.socket.socket.emit('chat get',$('#socket_inp_id_user').val(),$('#socket_inp_id_task').val());
                        }
                    },
                    {
                        title:'отправим статус о прочтении сообщений',
                        emit:'chat read',
                        params:['current_user_id','id_user','id_task'],
                        func:function(){
                            app_console.socket.socket.emit('chat read',$('#socket_inp_current_user_id').val(),$('#socket_inp_id_user').val(),$('#socket_inp_id_task').val());
                        }
                    },
                    {
                        title:'список всех не прочтенных',
                        emit:'chat not_read_user',
                        params:['id_user'],
                        func:function(){
                            app_console.socket.socket.emit('chat not_read_user',$('#socket_inp_id_user').val());
                        }
                    },
                    {
                        title:'отправка сообщения',
                        emit:'chat message',
                        params:['msg','to_user_id','from_user_id','id_task'],
                        func:function(){
                            app_console.socket.socket.emit('chat message',
                                $('#socket_inp_msg').val(),
                                $('#socket_inp_to_user_id').val(),
                                $('#socket_inp_from_user_id').val(),
                                $('#socket_inp_id_task').val()
                            );
                        }
                    },
                    {
                        title:'отликнуться на задание',
                        emit:'service_events perform_jobs',
                        params:['id_author','id_task','id_user'],
                        func:function(){
                            app_console.socket.socket.emit('service_events perform_jobs',
                                {
                                    id_author:$('#socket_inp_id_author').val(),
                                    id_task:$('#socket_inp_id_task').val(),
                                    id_user:$('#socket_inp_id_user').val()
                                }
                            );
                        }
                    },
                    {
                        title:'отказаться от задания (удалить отклик)',
                        emit:'service_events refused_jobs',
                        params:['id_author','id_task','id_user'],
                        func:function(){
                            app_console.socket.socket.emit('service_events refused_jobs',
                                {
                                    id_author:$('#socket_inp_id_author').val(),
                                    id_task:$('#socket_inp_id_task').val(),
                                    id_user:$('#socket_inp_id_user').val()
                                }
                            );
                        }
                    }
                ],
                events:[
                    {
                        title:'получение сообщения',
                        event:'chat message',
                        __return:function(msg,to_user_id,from_user_id,task_id){
                            var th = this;
                            var result = {
                                msg:msg,
                                to_user_id:to_user_id,
                                from_user_id:from_user_id,
                                task_id:task_id
                            };
                            app_console.socket.$result_container.jsonViewer(
                                result
                            );
                            app_console.socket.$socket_result_info.html('=====событие: '+th.title+'====='+
                            '<br>'+th.event);
                            console.log('=====событие: '+th.title+'=====');
                            console.log(th.event);
                            console.log(result);
                        }
                    },
                    {
                        title:'онлайн',
                        event:'chat online',
                        __return:function(user_id,users_id){
                            var th = this;
                            var result = {
                                user_id:user_id,
                                users_id:users_id
                            };
                            app_console.socket.$result_container.jsonViewer(
                                result
                            );
                            app_console.socket.$socket_result_info.html('=====событие: '+th.title+'====='+
                                '<br>'+th.event);
                            console.log('=====событие: '+th.title+'=====');
                            console.log(th.event);
                            console.log(result);
                        }
                    },
                    {
                        title:'оффлайн',
                        event:' chat offline',
                        __return:function(user_id,users_id){
                            var th = this;
                            var result = {
                                users_id:users_id
                            };
                            app_console.socket.$result_container.jsonViewer(
                                result
                            );
                            app_console.socket.$socket_result_info.html('=====событие: '+th.title+'====='+
                                '<br>'+th.event);
                            console.log('=====событие: '+th.title+'=====');
                            console.log(th.event);
                            console.log(result);
                        }
                    },
                    {
                        title:'получение сообщений',
                        event:'chat push',
                        __return:function(arr_chats){
                            var th = this;
                            var result = {
                                arr_chats:arr_chats
                            };
                            app_console.socket.$result_container.jsonViewer(
                                result
                            );
                            app_console.socket.$socket_result_info.html('=====событие: '+th.title+'====='+
                                '<br>'+th.event);
                            console.log('=====событие: '+th.title+'=====');
                            console.log(th.event);
                            console.log(result);
                        }
                    },
                    {
                        title:'список не прочитанных',
                        event:'chat not_read_user_get',
                        __return:function(result_arr){
                            var th = this;
                            var result = {
                                result_arr:result_arr
                            };
                            app_console.socket.$result_container.jsonViewer(
                                result
                            );
                            app_console.socket.$socket_result_info.html('=====событие: '+th.title+'====='+
                                '<br>'+th.event);
                            console.log('=====событие: '+th.title+'=====');
                            console.log(th.event);
                            console.log(result);
                        }
                    },
                    {
                        title:'Сообщения прочтены',
                        event:'chat read_success',
                        __return:function(users_id){
                            var th = this;
                            var result = {
                                users_id:users_id
                            };
                            app_console.socket.$result_container.jsonViewer(
                                result
                            );
                            app_console.socket.$socket_result_info.html('=====событие: '+th.title+'====='+
                                '<br>'+th.event);
                            console.log('=====событие: '+th.title+'=====');
                            console.log(th.event);
                            console.log(result);
                        }
                    },
                    {
                        title:'Пользователь отказался от задания',
                        event:'service_events refused_jobs_send',
                        __return:function(data){
                            var th = this;
                            var result ={
                                data:data
                            };
                            app_console.socket.$result_container.jsonViewer(
                                result
                            );
                            app_console.socket.$socket_result_info.html('=====событие: '+th.title+'====='+
                                '<br>'+th.event);
                            console.log('=====событие: '+th.title+'=====');
                            console.log(th.event);
                            console.log(result);
                        }
                    },
                    {
                        title:'Пользователь откликнулся на задание',
                        event:'service_events perform_jobs_send',
                        __return:function(data){
                            var th = this;
                            var result ={
                                data:data
                            };
                            app_console.socket.$result_container.jsonViewer(
                                result
                            );
                            app_console.socket.$socket_result_info.html('=====событие: '+th.title+'====='+
                                '<br>'+th.event);
                            console.log('=====событие: '+th.title+'=====');
                            console.log(th.event);
                            console.log(result);
                        }
                    }

                ]

            }


    };

    });
});