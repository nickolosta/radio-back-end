/**
 * Created by nick on 18.06.16.
 */
jQuery(function($){
    $(document).ready(function(){
        var socket = app_console.socket =
        {
            $socket_result_info:$('#socket_result_info'),
            $result_container:$('#socket_result'),
            socket:null,
            render:function(){
                var html = '';
                var $container = $('#socket_select_route');
                var data = app_console.data.socket.emit;
                for(var d=0;d<data.length;d++){
                    html+='<option value="'+d+'">'+data[d].title+'</option>';
                }
                $container.html(html);
                socket.events_list();
            },
            render_inputs:function (params,index,data) {
                var $container = $('#socket_fields');
                var html='';
                $('#emit_name').html(' '+data.emit);
                for(var p=0;p<params.length;p++){
                    html+='<input id="socket_inp_'+params[p]+'" type="text" name="'+params[p]+'" placeholder="'+params[p]+'"><br><br>';
                }
                html+='<a data-index="'+index+'" id="socket_send_data">Отправить</a>';
                $container.html(html);
                socket.events_input();
            },
            events_input:function(){
                $('#socket_send_data').on('click',function(e){
                    e.stopPropagation();
                    var $ob = $(this);
                    var index = $ob.attr('data-index');
                    app_console.data.socket.emit[index].func();
                });
            },
            events_list:function () {
                (function(){
                    var data =  app_console.data.socket.emit[0];
                    socket.render_inputs(data.params,0,data);
                })();
                $('#socket_select_route').change(function(e){
                    e.stopPropagation();
                    var $ob =$(this);
                    var index = parseInt($ob.val());
                    var data = app_console.data.socket.emit[index];
                    socket.render_inputs(data.params,index,data);
                });
            },
            events:function(){
                var events_data = app_console.data.socket.events;
                for(var e=0;e<events_data.length;e++){
                    socket.socket.on(events_data[e].event,events_data[e].__return.bind({title:events_data[e].title,event:events_data[e].event}));
                }
            },
            init:function(){
                socket.socket = io.connect(app_console.data.socket.connect);
                socket.render();
                socket.events();
            }
        };
        socket.init();
    });
});