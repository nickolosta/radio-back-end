/**
 * Created by nick on 23.05.16.
 */
jQuery(function($){
    $(document).ready(function(){
        var Get = app_console.get = {
            // рендер инпутов
            render_inputs:function (inputs,index) {
                var $container =$('#get_fields');
                var html ='';
                var data = app_console.data.get[index];
                if(typeof data.title!='undefined'){
                    if(typeof data.desc!='undefined'){
                        html+='<p>'+data.desc+'</p>';
                    }
                }
                for(var d=0;d<inputs.length;d++){
                    html+=inputs[d]+': <input data-index="'+index+'" class="get_inputs" name="'+inputs[d]+'" type="text"><br><br>';
                }
                html+='<a id="get_send_data" data-index="'+index+'">Отправить</a>';
                $container.html(html);
                Get.events_input();
            },
            // валидация инпутов
            validate_input:function(){
                var valid = true;
                $('.get_inputs').each(function(){
                    var $ob = $(this);
                    if($ob.val().trim().length==0){
                        valid = false;
                    }
                });
                return valid;
            },
            //события инпутов
            events_input:function(){
                $('#get_send_data').on('click',function(e){
                    e.stopPropagation();
                    $('#get_result').html('');
                    if(Get.validate_input()){
                        var url = $('#URL_res').val();
                        // установим токены если есть
                        var token = $('#login-token').val();
                        var token_user_id = $('#login-token-user').val();
                        if(token.length>0 && token_user_id>0){
                            $.ajaxSetup({
                                headers: { 'login-token' : token,'login-token-user':token_user_id }
                            });
                        }
                        $.ajax({
                            url:url,
                            method: "GET",
                            success:function(Data,status, xhr){
                                var ct = xhr.getResponseHeader("content-type") || "";
                                if (ct.indexOf('html') > -1) {
                                    $('#get_result').html('выводит html страницу');
                                }
                                if (ct.indexOf('json') > -1) {
                                    $('#get_result').jsonViewer(Data);
                                }
                                if (ct.indexOf('image') > -1) {
                                    $('#get_result').html('<img>');

                                    $("#get_result img").attr("src",$('#URL_res').val());
                                }
                                if (ct.indexOf('application') > -1 && ct.indexOf('json') == -1) {
                                    var win = window.open(url, '_blank');
                                    if(win){
                                        //Browser has allowed it to be opened
                                        win.focus();
                                    }else{
                                        //Broswer has blocked it
                                        alert('Разрешите всплывание окна');
                                    }
                                }

                            }
                        });
                    }
                });
                //ввод значений в инпуте
                $('.get_inputs').keydown(function(e){
                    e.stopPropagation();
                    var index = $(this).attr('data-index');
                    var pattern = app_console.data.get[index].url;
                    $('.get_inputs').each(function(){
                        var $ob = $(this);
                        var name = $ob.attr('name');
                        setTimeout(function () {
                            var val = $ob.val().trim();
                            if(val.length>0){
                                pattern = pattern.replace('{'+name+'}',val);
                                $('#URL_res').val(pattern);
                            }
                        },100);
                    });
                });
            },
            // рендер общий
            render:function(){
                var data = app_console.data.get;
                var html ='';
                var $container =$('#get_select_route');
                for(var d=0;d<data.length;d++){
                    if(typeof data[d].title!='undefined'){
                        html+='<option class="select_get" value="'+d+'">'+data[d].title+'</option>';
                    }else{
                        html+='<option class="select_get" value="'+d+'">'+data[d].desc+'</option>';
                    }
                }
                $container.html(html);
                Get.events();
            },
            //
            events:function(){
                (function(){
                    var data = app_console.data.get[0];
                    $('#URL_res').val(data.url);
                    Get.render_inputs(data.fields,0);
                })();
                $('#get_select_route').change(function(e){
                    e.stopPropagation();
                    var $ob =$(this);
                    var index = parseInt($ob.val());
                    var data = app_console.data.get[index];
                    $('#URL_res').val(data.url);
                    Get.render_inputs(data.fields,index);
                });

            },
            init:function(){
                Get.render();
            }
        };
        Get.init();
    });
});