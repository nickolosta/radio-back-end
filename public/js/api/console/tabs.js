/**
 * Created by nick on 23.05.16.
 */
jQuery(function($){
    $(document).ready(function(){
        var Tabs = app_console.tabs = {
            events:function(){
                $('.tab_bt').on('click',function(e){
                    e.stopPropagation();
                    var $ob = $(this);
                    var tab_id = $ob.attr('tab');
                    $('.tab_bt').removeClass('active');
                    $ob.addClass('active');
                    $('.tab').removeClass('active');
                    $('#tab_'+tab_id).addClass('active');

                    if(tab_id=='get'){
                        var index = $('#get_select_route').val();
                        var data = app_console.data.get[index];
                        $('#URL_res').val(data.url);
                        $('.get_inputs').keydown();
                    }else
                    if(tab_id=='post'){
                        var index = $('#post_select_route').val();
                        var data = app_console.data.post[index];
                        $('#URL_res').val(data.url);
                    }else{
                        
                    }

                });
            },
            init:function(){
                Tabs.events();
            }
        };
        Tabs.init();
    });
});