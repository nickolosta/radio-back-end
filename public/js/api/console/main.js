/**
 * Created by nick on 23.05.16.
 */
jQuery(function($){
    $(document).ready(function(){
        app_console = {
            events:function(){
                $('#reset_token').on('click',function(e){
                    e.stopPropagation();
                    $('#login-token').val('');
                    $('#login-token-user').val('');
                });
            },
            init:function(){
                $.ajaxSetup({
                    headers: { 'X-CSRF-Token' : $('meta[name=_token]').attr('content'),
                        'login-token':'111'
                    }
                });

                app_console.events();
            }
        };
        app_console.init();
    });
});